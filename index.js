import express from "express"
import axios from "axios"
import bodyParser from "body-parser";

const app = express();
const port = 3000;
const API_URL = "https://api.jikan.moe/v4/manga"

app.use(express.static('public'));
app.use(bodyParser.urlencoded({ extended: true }));

app.get("/", (req, res) => {
    res.render("index.ejs");
});

app.post("/get-manga", async (req, res) => {
    const mangaID = req.body.mangaId;
    try {
        const result = await axios.get(`${API_URL}/${mangaID}/full`);
        const data = result.data['data'];

        let malLink = data['url'];
        let mangaImageURL = data['images']['jpg']['image_url'];
        let titleJapanese = data['title_japanese'];
        let titleEnglish = data['title_english'];
        let chapters = data['chapters'];
        let volumes = data['volumes'];
        let synopsis = data['synopsis'];
        let status = data['status'];
        let score = data['score'];


        res.render("index.ejs", {
            content: {
                malURL: malLink,
                imgURL: mangaImageURL,
                titleJP: titleJapanese,
                titleEN: titleEnglish,
                chapters: chapters,
                volumes: volumes,
                synopsis: synopsis,
                status: status,
                score: score,
            }
        });
    } catch (error) {
        res.render("index.ejs", { content: JSON.stringify(error.message.data) });
    }
});

app.listen(port, () => {
    console.log(`Listening to ${port}`);
});